package krio.render;

import krio.MyPanel;
import krio.draw.WorldDrawer;
import krio.world.World;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Used for rendering world into internal frame array
 * for future replay
 */
public class MyRenderer {
    public ArrayList<BufferedImage> data = new ArrayList<BufferedImage>();
    public volatile double done = 0;

    /**
     * renders world into data array
     * @param world given world
     * @param framesCount frames count to render
     * @param dt time between two frames in milliseconds
     */
    public void render(World world, int framesCount, int dt) {
        for (int i = 0; i < framesCount; ++i) {
            world.run(dt);
            BufferedImage img = new BufferedImage(MyPanel.WIDTH, MyPanel.HEIGHT, BufferedImage.TYPE_4BYTE_ABGR);
            WorldDrawer.draw((Graphics2D) img.getGraphics(), world);
            data.add(img);
            done = (double) i / (double) (framesCount - 1);
        }
    }
}
