package krio;

import java.util.HashMap;
import java.util.Properties;

public class Configuration {
    public static HashMap properties = getDefault();

    private static HashMap getDefault() {
        HashMap pr = new HashMap();

        //==================== World ========================
        pr.put("particleRad",  2.0);
        pr.put("bufferAccuracy", 1.5 / (Double)pr.get("particleRad"));
        pr.put("particleAmount",  6000);
        pr.put("dtRecalcIterations",  4);

        //==================== Physics ========================
        pr.put("G",  0.015);
        pr.put("timeSpeed",  0.10);
        pr.put("energyLoss",  0.999);
        pr.put("heatDiffusionSpeed",  0.001);
        pr.put("heatTransferSpeed",  0.01);
        pr.put("heatUpwardForce",  0.08);
        pr.put("heatIfLow",  0.3);
        pr.put("physicsAccuracy",  1.0);
        pr.put("speedLimit",  3.0);
        pr.put("pressureEffect",  -0.5); // not used anymore

        return pr;
    }
}
