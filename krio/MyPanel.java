package krio;
import krio.draw.ParticleDrawer;
import krio.draw.WorldDrawer;
import krio.render.MyRenderer;
import krio.thread.Spinlock;
import krio.world.World;
import krio.world.physics.CollisionSolver;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.atomic.AtomicInteger;
import javax.swing.*;

/**
 * Object containing {@link World} <p>
 * Has internal timer which is calling {@link MyPanel#paint(Graphics)} and {@link World#run(double)}
 */
public class MyPanel extends JPanel implements ActionListener {
    /** Screen width */
    public static int WIDTH  = 800;
    /** Screen height */
    public static int HEIGHT = 700;
    /** lock for synchronized change of {@link CollisionSolver#solvedCount} */
    public static volatile Object lock = new Object();
    World world;
    /** mouse coordinates inside window */
    Point mouseCoords = new Point(0,0);
    /** Timer, calling {@link MyPanel#paint(Graphics)} and {@link World#run(double)} with given fps */
    Timer timer;
    /** fps to redraw window */
    int fps = 20;
    /** used calculate "smoothened" fps */
    long start = 1;
    /** used calculate "smoothened" fps */
    long elapsed = fps;
    /** smooth fps value - average fps value from few last frames */
    double smoothFpsCount = fps;
    /** 'Screen' image which is drawn on screen with given fps
     * <p>Used as buffer for drawing new image on top of the previous instead of drawing fully new
     */
    BufferedImage img = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_4BYTE_ABGR);
    MyRenderer rend = new MyRenderer();
    /** frames amount to render */
    int ind = 0;
    /** renders image instead of real-time drawing if True */
    boolean render = false;

    public MyPanel() {
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setBackground(Color.BLACK);//GRAY.darker().darker().darker());
        world = new World();
        
        timer = new Timer(1000 / fps, this);

        // updates mouse coords if mouse is moved
        this.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseMoved(MouseEvent me)
            {
                mouseCoords.x = me.getX();
                mouseCoords.y = me.getY();
            }
        });
        // listens mouse buttons actions
        this.addMouseListener(new MouseListener() {
            public void mousePressed(MouseEvent me) {
                ParticleDrawer.mode = ParticleDrawer.Mode.values()[((ParticleDrawer.mode.ordinal() + 1) % ParticleDrawer.Mode.values().length)];
            }
            public void mouseReleased(MouseEvent me) { }
            public void mouseEntered(MouseEvent me) { }
            public void mouseExited(MouseEvent me) { }
            public void mouseClicked(MouseEvent me) { }
        });

        timer.start();
        if (render) {
            Thread rendThread = new Thread(() -> rend.render(world, 200, 20));
            rendThread.start();
        }
    }

    @Override
    public void actionPerformed(ActionEvent a) {
        repaint();
        if (!render) {
            world.run(30);
            WorldDrawer.draw((Graphics2D) img.getGraphics(), world);
        }
    }

    @Override
    public void paint(Graphics gr) {
        super.paint(gr);
        Graphics2D g2d = (Graphics2D) gr;

        if (render) {
            if (rend.done != 1.0) {
                ((Graphics2D) gr).setBackground(Color.DARK_GRAY);
                gr.setColor(Color.LIGHT_GRAY);
                gr.drawRect(WIDTH / 2 - 200, HEIGHT / 2 - 50, 400, 30);
                gr.fillRect(WIDTH / 2 - 195, HEIGHT / 2 - 45, (int)(390 * rend.done), 20);
                gr.setFont(new Font("Arial", 10, 30));
                gr.drawString("PROCESSING DATA", WIDTH / 2 - 200, HEIGHT / 2 - 80);
                return;
            }
            gr.drawImage(rend.data.get(ind++ % rend.data.size()), 0, 0, this);
            return;
        }

        g2d.drawImage(img, 0, 0, this);
        Graphics2D g = img.createGraphics();
        g.setColor(new Color(0, 0, 0, 1.0f));
        g.fillRect(0,0,WIDTH,HEIGHT);
        g2d.setPaint(Color.WHITE);
        smoothFpsCount = smoothFpsCount * 0.9 + 0.1 * 1_000_000_000.0 / ((double)elapsed);
        g2d.drawString("real fps: " + 1_000_000_000.0 / ((double)elapsed), 30, 20);
        g2d.drawString("average fps: " + smoothFpsCount, 30, 40);
        g2d.drawString("Particles drawn: " + world.particleAmount, 30, 60);
        g2d.drawString("Collisions solved: " + CollisionSolver.solvedCount, 30, 80);
        elapsed = System.nanoTime() - start;
        start = System.nanoTime();
    }
}
