package krio.thread;

import krio.MyPanel;
import krio.buffer.NeighboursBuffer;
import krio.world.BufferedParticle;
import krio.world.physics.CollisionSolver;

import java.util.HashMap;
import java.util.List;

/** Creates new deamon-like thread with the launch(***) function */
public class MyThread {
    public volatile boolean startFlag;
    private Thread thread;

    // public ================================================
    /** Function starts new thread with launch(***) function inside
     * @param particles all particles
     * @param buffer buffer data (contains matrix with all approximate particles positions)
     * @param  idSortedList contains all particles ID-s sorted by position's x-coordinate
     * @param start position in given idSortedList from where to start solving
     * @param end position in given idSortedList where to end solving
     * */
    public MyThread(HashMap<Integer, BufferedParticle> particles, NeighboursBuffer buffer, List<Integer> idSortedList, int start, int end) {
        thread = new Thread(() -> launch(particles, buffer, idSortedList, start, end));
        thread.start();
    }

    // private ===============================================
    /**
     * Function executes collision solver on given data if {@link MyThread#startFlag} == true,
     * else just checking {@link MyThread#startFlag}
     * @see MyThread#MyThread(HashMap, NeighboursBuffer, List, int, int) MyThread
     * */
    private void launch(HashMap<Integer, BufferedParticle> particles, NeighboursBuffer buffer, List<Integer> idSortedList, int start, int end) {
        while (true) {
            if (startFlag) {
                CollisionSolver.solve(particles, buffer, idSortedList, start, end);
                startFlag = false;
            }
        }
    }
}
