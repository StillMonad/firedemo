package krio.thread;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Mutex-like lock used for reliable low-latency lock acquiring
 * probably an overkill in context of java6 adaptive spinning
 * but still should be more low-latency reliable
 * */
public class Spinlock {
    AtomicBoolean locked = new AtomicBoolean(false);

    /** waits until lock is free and locks it */
    public void lock() {
        while (!locked.compareAndSet(false, true));
    }

    /** unlocks the lock */
    public void unlock() { locked.set(false); }
}

