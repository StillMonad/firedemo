package krio.thread;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Mutex-like lock used for reliable low-latency lock acquiring
 * probably an overkill in context of java6 adaptive spinning
 * but still should be more low-latency reliable
 * <p>This one is using FetchAndAdd instead of CompareAndSet</p>
 * */
public class SpinlockFAA {
    AtomicInteger ticketNumber = new AtomicInteger(0);
    AtomicInteger turn = new AtomicInteger(0);

    public void lock() {
        int myTurn = ticketNumber.addAndGet(1) - 1;
        while (myTurn != turn.get());
    }

    public void unlock() { turn.addAndGet(1); }
}
