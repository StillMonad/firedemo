package krio.thread;

import java.util.ArrayList;

/**
 * Used for controlling all the threads in a {@link MyTreadPool#pool}
 * @see MyThread
 */
public class MyTreadPool {
    private ArrayList<MyThread> pool;

    // public ================================================
    public MyTreadPool(ArrayList<MyThread> thrList) {
        pool = thrList;
    }

    /** Sets {@link MyThread#startFlag} to true for every thread in {@link MyTreadPool#pool} */
    public void launch() {
        for (MyThread thread : pool) thread.startFlag = true;
    }

    /** checks if every thread in pool finished collision solving
     * @return every thread in the pool finished collision solving
     */
    public boolean isReady() {
        boolean allReady = true;
        for (MyThread thread : pool) allReady = allReady && !thread.startFlag;
        return allReady;
    }
}
