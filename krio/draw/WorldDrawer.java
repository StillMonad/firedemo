
package krio.draw;

import krio.world.BufferedParticle;
import krio.world.World;

import java.awt.Graphics2D;
import java.util.Collections;
import java.util.Comparator;

/** used for drawing {@link krio.world.World} */
public class WorldDrawer {
    /**
     * draws world on given Graphics2d
     * @param g where to draw
     * @param w world to draw
     * @see World
     */
    public static void draw(Graphics2D g, World w) {
        // sort particles by temperature for drawing
        // particles with higher temperature on top
        Collections.sort(w.temperatureSortedList, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return Double.compare(w.particles.get(i1).T, w.particles.get(i2).T);
            }
        });

        for (int key : w.temperatureSortedList) {
            BufferedParticle p = w.particles.get(key);
            ParticleDrawer.draw(g, p);
        }
    }
}
