package krio.buffer;

/** Iterator for {@link PositionBuffer#getBuffer()}
 * used for iterating through data and returning just values > 0
 * @deprecated cause iterating through 2d array is O(n^2). Using {@link krio.world.World#idSortedList} instead.
 * */

public class BufferIterator {
    private int posX;
    private int posY;
    private int[][] ref;

    public BufferIterator(int[][] arr) {
        ref = arr;
        posX = 0;
        posY = 0;
    }

    public BufferIterator(int[][] arr, int x, int y) {
        ref = arr;
        posX = x;
        posY = y;
    }

    public int next() {
        for (; posX < ref.length; ++posX) {
            for (; posY < ref[posX].length; ++posY) {
                if (ref[posX][posY] != 0) {
                    return ref[posX][posY++];
                }
            }
            posY = 0;
        }
        return -1; // -1 is end of buffer
    }
}
