package krio.buffer;

import static krio.util.Clip.clip;

import java.util.HashSet;

/**
 * matrix containing approximate positions of all particles
 * used to simplify search for neighbours (particles close to each other)
 */
public class NeighboursBuffer extends PositionBuffer {
    
    // public ================================================
    public NeighboursBuffer(int screenW, int screenH, int buffW, int buffH) {
        super(screenW, screenH, buffW, buffH);
    }

    /**
     * Used to find all particle neighbours in given distance
     * <p>Complexity is O(n^2), where n is 2 * dist
     * @param px particle x position in buffer <p>see: {@link krio.world.BufferedParticle#bufferedPos}
     * @param py particle y position in buffer <p>see: {@link krio.world.BufferedParticle#bufferedPos}
     * @param dist distance (radius) of search to be done in square from (-radius, -radius) to (+radius, +radius)
     * @return list of all found neighbours in given distance
     */
    public HashSet<Integer> getNeighbours(int px, int py, int dist) {  // px, py - buffer-sized coords
        HashSet<Integer> neighbours = new HashSet<>();
        int[][] buff = getBuffer();
        
        for(int x = clip((px - dist), 0, BUFFER_WIDTH); x < clip((px + dist), 0, BUFFER_WIDTH); ++x){
            for(int y = clip((py - dist), 0, BUFFER_HEIGHT); y < clip((py + dist), 0, BUFFER_HEIGHT); ++y){
                if (buff[x][y] != 0 && (buff[x][y] != buff[px][py]))  {
                    neighbours.add(buff[x][y]);
                    //System.out.println(px + " " + py + " : " + buff[x][y]);
                }
            }
        }
        return neighbours;
    }
}
