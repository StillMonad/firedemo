package krio.buffer;

import krio.world.Particle;

import java.awt.Point;

/**
 * matrix containing approximate positions of all particles
 * @see NeighboursBuffer
 */
public class PositionBuffer {
    protected int BUFFER_HEIGHT;
    protected int BUFFER_WIDTH;

    private int SCREEN_HEIGHT;
    private int SCREEN_WIDTH;
    
    private double resizeW;
    private double resizeH;

    /** 2d integer matrix containing positions of all particles */
    private int[][] buffer;

    /**
     * Creating new buffer of given width and height
     * @param screenW screen width ({@link krio.MyPanel#WIDTH})
     * @param screenH screen height ({@link krio.MyPanel#HEIGHT})
     * @param buffW buffer width to be created
     * @param buffH buffer height to be created
     */
    PositionBuffer(int screenW, int screenH, int buffW, int buffH) {
        SCREEN_WIDTH  = screenW;
        SCREEN_HEIGHT = screenH;
        BUFFER_WIDTH  = buffW;
        BUFFER_HEIGHT = buffH;
        
        resizeW = (double) BUFFER_WIDTH / (double) SCREEN_WIDTH;
        resizeH = (double) BUFFER_HEIGHT / (double) SCREEN_HEIGHT;
        
        buffer = new int[BUFFER_WIDTH][BUFFER_HEIGHT];
    }

    /** Clears all buffer data */
    public void reset() {
        resizeW = (double) BUFFER_WIDTH / (double) SCREEN_WIDTH;
        resizeH = (double) BUFFER_HEIGHT / (double) SCREEN_HEIGHT;

        buffer = new int[BUFFER_WIDTH][BUFFER_HEIGHT];
    }
    /** Adds given particle to a buffer with approximate position
     * @see NeighboursBuffer#getApproxPosition(double, double)  */
    public Point add(Particle p) {
        if (Math.round(p.pos.x) > (SCREEN_WIDTH) || Math.round(p.pos.x) < 0) return null;
        if (Math.round(p.pos.y) > (SCREEN_HEIGHT) || Math.round(p.pos.y) < 0) return null;
        
        Point buffPos = getApproxPosition(p.pos.x, p.pos.y);
        buffer[buffPos.x][buffPos.y] = p.id;

        return buffPos;
    }
    /** Returns buffer data
     * @see PositionBuffer#buffer */
    public int[][] getBuffer() {
        return buffer;
    }

    // private ==============================================
    /**
     * Calculating approximate particle position to add in buffer
     * like newPos(round(x), round(y))
     */
    private Point getApproxPosition(double x, double y) {
        Point result = new Point();
        
        result.x = (int)Math.min(Math.max(Math.round(x * resizeW), 0.0), BUFFER_WIDTH  - 1);
        result.y = (int)Math.min(Math.max(Math.round(y * resizeH), 0), BUFFER_HEIGHT - 1);
        
        return result;
    }
}
