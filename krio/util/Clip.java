package krio.util;

public class Clip {
    /**
     * clips given {@link Number} arg to (min <= arg <= max)
     * @param a value to clip
     * @param min minimal value
     * @param max maximum value
     * @param <T> extends {@link Number}
     * @return value clipped
     */
    public static <T extends Number> T clip(T a, T min, T max) {
        return a.doubleValue() > min.doubleValue() ? (a.doubleValue() < max.doubleValue() ? a : max) : min;
    }
}
