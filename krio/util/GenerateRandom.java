package krio.util;
import java.util.Random;

/**
 * used for repeatability of the world generation
 */
public class GenerateRandom {
    /** Seed to use in pseudo-random generator
     * @see Random#Random(long)  Random*/
    public static long seed = 1;
    private static Random rnd = new Random(seed);

    /**
     * used to get pseudo-random int between min and max
     * @param min minimal value
     * @param max maximal value
     * @return value between min and max
     */
    public static int getRandomInt(int min, int max) {
        return rnd.nextInt((max - min) + 1) + min;
    }

    /**
     * used to get pseudo-random double between min and max
     * @param min minimal value
     * @param max maximal value
     * @return value between min and max
     */
    public static double getRandomDouble(double min, double max) {
        return min + (max - min) * rnd.nextDouble();
    }
}
