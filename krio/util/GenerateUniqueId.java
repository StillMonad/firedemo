package krio.util;

/**
 * used for Generating unique IDs for {@link krio.world.Particle}
 * <p>
 * ID is unique particle identifier used as key inside {@link krio.world.World#particles}
 */
public class GenerateUniqueId {
    private static int id = 1;
    static public int getUnique() {
        return id++;
    }
}
