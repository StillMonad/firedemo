package krio.world;

import krio.util.GenerateUniqueId;
import krio.util.Vec2;

import java.awt.Color;

/**
 * Contains all particle information
 * <p> such as Velocity, positions, mass, acceleration
 * <p> Particle is as spherical object used in physics/collisions calculations in
 * <p> {@link krio.world.physics.CollisionSolver}
 */
public class Particle {
    public Vec2 pos;
    public Vec2 prevPos;
    public Vec2 vel;
    public Vec2 acc;
    public double mass;
    public double radius;
    public Color color;
    public double T = 0.0;
    public final int id; // unique id

    /**
     *
     * @param x initial position x
     * @param y initial position y
     * @param vx initial velocity x
     * @param vy initial velocity y
     * @param rad particle radius
     * @param m particle mass
     */
    Particle(double x, double y, double vx, double vy, double rad, double m) {
        pos = new Vec2(x, y);
        vel = new Vec2(vx, vy);
        acc = new Vec2();
        mass = m;
        radius = rad;
        id = GenerateUniqueId.getUnique();
        color = Color.RED;
    }

    /**
     * Changes particle position using it's acceleration, velocity and dt
     * @param dt time delta
     */
    public void move(double dt) {
        vel = Vec2.add(vel, Vec2.mul(acc, dt));
        pos = Vec2.add(Vec2.add(pos, Vec2.mul(vel, dt)), Vec2.mul(acc, 0.5 * dt * dt));
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Particle other = (Particle) obj;
        return this.id == other.id;
    }
}
