package krio.world.physics;

import krio.world.BufferedParticle;
import krio.world.Particle;
import krio.util.Vec2;

import java.util.HashMap;
import java.util.List;

import static krio.util.Clip.clip;


// NOT USING THIS anymore.
/**
 * Using {@link PressureMap} to calculate pressure forces
 * @deprecated Collision physics is already implicitly simulating pressure <p> No need to spend resources on doing this again
 */
public class PressureSolver {
    static int pointsToCheck = 8; //each particle will check pointsToCheck directions for the lowest pressure

    // public ================================================
    public static void solve(HashMap<Integer, BufferedParticle> particles, List<Integer> idSortedList, PressureMap map) {
        for (int key : idSortedList) {
            BufferedParticle p1 = particles.get(key);
            solvePressure(map, p1);
        }
    }

    // private ===============================================
    private static void solvePressure(PressureMap map, Particle p) {
        double ang = 2.0 * Math.PI / pointsToCheck;
        Vec2 acc = new Vec2();
        for (int i = 0; i < pointsToCheck; ++i) {
            Vec2 dir = new Vec2(Math.sin(ang * i) * p.radius, Math.cos(ang * i) * p.radius);
            Vec2 point = Vec2.add(Vec2.mul(p.pos, map.sizeMult), dir);
            point.x = clip((int)point.x, 0, map.x - 1);
            point.y = clip((int)point.y, 0, map.y - 1);
            dir = Vec2.normalize(dir);
            double power = -0.00001 * (double)map.map.getRGB((int)point.x, (int)point.y) * Physics.pressureEffect;
            dir = Vec2.mul(dir, power);
            acc = Vec2.add(acc, dir);
        }

        /*for (int i = 0; i < pointsToCheck; ++i) {
            Vec2 dir = new Vec2(Math.sin(ang * i) * map.partSize, Math.cos(ang * i) * map.partSize);
            Vec2 point = Vec2.add(Vec2.mul(p.pos, map.sizeMult), dir);
            point.x = clip((int)point.x, 0, map.x - 1);
            point.y = clip((int)point.y, 0, map.y - 1);
            dir = Vec2.normalize(dir);
            double power = -0.00001 * (double)map.map.getRGB((int)point.x, (int)point.y) * Physics.pressureEffect;
            dir = Vec2.mul(dir, power);
            acc = Vec2.add(acc, dir);
        }*/
        p.acc = Vec2.sub(p.acc, acc);
    }
}
