package krio.world.physics;

import krio.world.BufferedParticle;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Ellipse2D;
import krio.util.Vec2;
import krio.world.World;

/**
 * Structure used for storing pressure values as a 2-color Bitmap <p>
 * @deprecated Collision physics is already implicitly simulating pressure <p> No need to spend resources on doing this again
 */
public class PressureMap {
    public BufferedImage map;
    public int partSize = (int)(World.particleRad * 2);
    public int x, y;
    public double sizeMult;

    public PressureMap (int x, int y, double sizeMult) {
        this.x = x;
        this.y = y;
        this.sizeMult = sizeMult;
        map = new BufferedImage(x, y, BufferedImage.TYPE_USHORT_GRAY);
    }

    public void fillMap(HashMap<Integer, BufferedParticle> particles) {
        map = new BufferedImage(x, y, BufferedImage.TYPE_USHORT_GRAY);
        Graphics2D gr = (Graphics2D) map.getGraphics();
        gr.setPaint(new Color(1f, 1f, 1f, 0.1f));

        int passEvery = 2;
        int i = 0;
        for (Integer ind : particles.keySet()) {
            //if (i % passEvery == 0) continue;
            ++i;
            BufferedParticle p = particles.get(ind);
            Vec2 scaledPos = Vec2.mul(p.pos, sizeMult);
            //gr.fillRect((int)scaledPos.x - partSize/2, (int)scaledPos.y - partSize/2, (int)partSize, (int)partSize);
            //gr.fillOval((int)scaledPos.x - partSize/4, (int)scaledPos.y - partSize/4, (int)partSize/2, (int)partSize/2);
            //gr.fillOval((int)scaledPos.x - partSize/8, (int)scaledPos.y - partSize/8, (int)partSize/4, (int)partSize/4);
            Point center = new Point((int) (p.pos.x * sizeMult), (int) (p.pos.y * sizeMult));
            float[] dist = {0.0f, 0.5f, 1.0f};
            Color[] colors = {new Color(1 , 1 , 1, 0.5f),
                              new Color(1, 1, 1, 0.05f),
                              new Color(0, 0, 0, 0)
            };
            RadialGradientPaint gradient =
                    new RadialGradientPaint(center, partSize, dist, colors);
            gr.setPaint(gradient);
            gr.fill(new Ellipse2D.Double(
                    p.pos.x * sizeMult - partSize,
                    p.pos.y * sizeMult - partSize,
                    partSize * 2,
                    partSize * 2));
        }
    }
}
