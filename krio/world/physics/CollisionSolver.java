package krio.world.physics;

import krio.MyFrame;
import krio.MyPanel;
import krio.buffer.NeighboursBuffer;
import krio.util.Vec2;
import krio.world.BufferedParticle;
import krio.world.Particle;
import krio.world.World;

import java.util.*;

import static krio.util.GenerateRandom.getRandomDouble;

public class CollisionSolver {
    public static volatile int solvedCount = 0;
    public static boolean solveAsVertlet = false;

    // public ================================================
    /** new version of solver
     * <p>solves all collisions between given particles
     * <p>complexity is O(n) where n is particle amount
     * @param particles all particles <p>see: {@link World#particles}
     * @param buffer buffer data (contains matrix with all approximate particles positions) <p>see: {@link NeighboursBuffer}
     * @param  idSortedList contains all particles ID-s sorted by position's x-coordinate
     * @param start position in given idSortedList from where to start solving
     * @param end position in given idSortedList where to end solving
     */
    public static void solve(HashMap<Integer, BufferedParticle> particles, NeighboursBuffer buffer, List<Integer> idSortedList, int start, int end) {
        for (int key = start; key < end; ++key) {
            BufferedParticle p1 = particles.get(key);
            if (p1 == null || !p1.isBuffered) continue;
            HashSet<Integer> ns = buffer.getNeighbours(p1.bufferedPos.x, p1.bufferedPos.y, (int)(2.0 * p1.radius * Physics.physicsAccuracy * World.bufferAccuracy));
            //System.out.println(idSortedList.size() + " : " + ns.size());
            for (Integer ind : ns) {
                BufferedParticle p2 = particles.get(ind);
                if (key == ind) continue;
                //boolean collision = !solveAsVertlet ? solveCollision(p1, p2) : solveCollisionVertlet(p1, p2);
                boolean collision = solveCollision(p1, p2);
                if (collision) {
                    synchronized (MyPanel.lock) {
                        ++solvedCount;
                    }
                    Physics.solveHeat(p1, p2);
                 }
            }
            ns.add(p1.id);
            recalcBufferPositions(particles, buffer, ns);
        }
    }

    /** old version for performance comparsion
     * <p>solves all collisions between given particles
     * <p>complexity is O(n^2) where n is particle amount
     * */
    public static void solve(HashMap<Integer, BufferedParticle> particles) {
        Object[] idList = particles.keySet().toArray();
        for (Object key : idList) {
            BufferedParticle p1 = particles.get(key);
            for (Object key2 : idList) {
                BufferedParticle p2 = particles.get(key2);
                if (key == key2) continue;
                //boolean collision = !solveAsVertlet ? solveCollision(p1, p2) : solveCollisionVertlet(p1, p2);
                boolean collision = solveCollision(p1, p2);
                if (collision) {
                    Physics.solveHeat(p1, p2);
                }
            }
        }
    }

    // private ===============================================

    /**
     * Solves collision between two given particles
     * @return true if collision exists, else false
     */
    private static boolean solveCollision(Particle p1, Particle p2) {
        // direction = p1.pos - p2.pos;
        Vec2 dir = Vec2.sub(p1.pos, p2.pos);
        // dist = length(dir);
        double dist = Vec2.len(dir);

        // if distance == 0 (it wight happen on generation) create new random direction vector
        if(dist == 0) {
            dir = new Vec2(getRandomDouble(-10, 10), getRandomDouble(-10, 10));
        }

        // distance is larger than sum off radiuses => collision does not exist, return false
        if (dist > p1.radius + p2.radius) return false;

        // offs - distance of intersection
        double offs = dist - p1.radius - p2.radius;
        // norm - normalized direction
        Vec2 norm = Vec2.normalize(dir);

        // p1.pos = p1.pos + norm * -0.51 * offs
        p1.pos = Vec2.add(p1.pos, Vec2.mul(norm ,-0.51 * offs));
        // p2.pos = p2.pos + norm * 0.51 * offs
        p2.pos = Vec2.add(p2.pos, Vec2.mul(norm,  0.51 * offs));

        // frontal velocity is a projection of velocity on direction
        Vec2 pFrontVel = Vec2.project(p2.vel, dir);
        // tangential velocity = velocity - frontalVelocity;
        Vec2 pTangVel = Vec2.sub(p2.vel, pFrontVel);

        // frontal velocity is a projection of velocity on direction
        Vec2 thisFrontVel = Vec2.project(p1.vel, dir);
        // tangential velocity = velocity - frontalVelocity;
        Vec2 thisTangVel = Vec2.sub(p1.vel, thisFrontVel);

        /*
        Main concept on collision is:
        * tangential velocities of each ball stays the same,
        * frontal velocities changed with the rules of impulse and energy conservation according to the formula:
            NewFrontalVelocity1 = (energyLoss * (m1 - m2) * V1 + 2 * m2 * V2) / (m1 + m2);
            NewFrontalVelocity2 = (energyLoss * (m2 - m1) * V2 + 2 * m1 * V1) / (m1 + m2);
        where:
            energyLoss - energy lost on collision due friction
            m1, m1 - masses of the balls
            V1, V2 - velocities of the balls
         */
        Vec2 thisFrontVelNew = new Vec2();
        thisFrontVelNew.x = Physics.energyLoss * ((p1.mass - p2.mass) * thisFrontVel.x + 2 * p2.mass * pFrontVel.x) / (p1.mass + p2.mass);
        thisFrontVelNew.y = Physics.energyLoss * ((p1.mass - p2.mass) * thisFrontVel.y + 2 * p2.mass * pFrontVel.y) / (p1.mass + p2.mass);

        Vec2 pFrontVelNew = new Vec2();
        pFrontVelNew.x = Physics.energyLoss * ((p2.mass - p1.mass) * pFrontVel.x + 2 * p1.mass * thisFrontVel.x) / (p1.mass + p2.mass);
        pFrontVelNew.y = Physics.energyLoss * ((p2.mass - p1.mass) * pFrontVel.y + 2 * p1.mass * thisFrontVel.y) / (p1.mass + p2.mass);

        // final velocities after collision
        // newVelocity = tangentialVel + frontalVel;
        p1.vel = Vec2.add(thisTangVel, thisFrontVelNew);
        p2.vel = Vec2.add(pTangVel, pFrontVelNew);

        return true;
    }

    /**
     * Solves collision between two given vertlet particles
     * @return true if collision exists, else false
     * @deprecated vertlet solution is very unstable on low distance of neighbours searching (see: {@link NeighboursBuffer#getNeighbours(int, int, int)}),
     * <p>becomes stable with increasing range of search, but it affects performance significantly
     */
    private static boolean solveCollisionVertlet(Particle p1, Particle p2) {
        Vec2 dir = Vec2.sub(p1.pos, p2.pos);
        double dist = Vec2.len(dir);
        double minDist = p1.radius + p2.radius;
        if (dist >= minDist) return false;

        double offs = minDist - dist;
        Vec2 norm = Vec2.normalize(dir);

        p1.pos = Vec2.add(p1.pos, Vec2.mul(norm ,0.51 * offs));
        p2.pos = Vec2.add(p2.pos, Vec2.mul(norm, -0.51 * offs));

        return true;
    }

    // private ============================================

    /**
     * Recalculates buffer positions for given neighbours (removes old positions and adds new)
     * @param particles all particles <p>see: {@link World#particles}
     * @param buffer buffer data (contains matrix with all approximate particles positions) <p>see: {@link NeighboursBuffer}
     * @param ns neighbours
     */
    private static void recalcBufferPositions(HashMap<Integer, BufferedParticle> particles, NeighboursBuffer buffer, HashSet<Integer> ns) {
        for (int part : ns) {
            BufferedParticle particle = particles.get(ns);
            if (particle != null) {
                buffer.getBuffer()[particle.bufferedPos.x][particle.bufferedPos.y] = 0;
                buffer.add(particle);
            }
        }
    }
}
