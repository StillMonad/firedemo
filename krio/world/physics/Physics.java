
package krio.world.physics;

import krio.Configuration;
import krio.MyPanel;
import krio.util.Vec2;
import krio.world.Particle;

import  static krio.util.Clip.clip;

import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;

public class Physics {
    /** gravity acceleration */
    public static double G                      = (Double)Configuration.properties.get("G");
    /** time speed modifier */
    public static double timeSpeed              = (Double)Configuration.properties.get("timeSpeed");
    /** energy loss on collision; <p> energy    = energy * energyLoss */
    public static double energyLoss             = (Double)Configuration.properties.get("energyLoss");
    /** particle temperature will decrease with time: <p>T -= dt * heatDiffusionSpeed; */
    public static double heatDiffusionSpeed     = (Double)Configuration.properties.get("heatDiffusionSpeed");
    /** heat transfer speed on collision between particles */
    public static double heatTransferSpeed      = (Double)Configuration.properties.get("heatTransferSpeed");
    /** upwards force for hot particles */
    public static double heatUpwardForce        = (Double)Configuration.properties.get("heatUpwardForce");
    /** */
    public static double heatIfLow              = (Double)Configuration.properties.get("heatIfLow");
    public static double physicsAccuracy        = (Double)Configuration.properties.get("physicsAccuracy");
    public static double speedLimit             = (Double)Configuration.properties.get("speedLimit");
    public static double pressureEffect         = (Double)Configuration.properties.get("pressureEffect"); // not used anymore

    // particles can't go outside the box, can you?
    /** Limits particle's position to screen's width and height */
    public static void applyConstraintsBox(Particle a) {
        Vec2 pos = a.pos;

        if (pos.x < a.radius || pos.x + a.radius > MyPanel.WIDTH) a.vel = Vec2.reflect(Vec2.mul(a.vel, energyLoss), new Vec2(1, 0));
        if (pos.y < a.radius || pos.y + a.radius > MyPanel.HEIGHT) a.vel = Vec2.reflect(Vec2.mul(a.vel, energyLoss), new Vec2(0, 1));
        
        a.pos.x = clip(a.pos.x, a.radius, MyPanel.WIDTH - a.radius);
        a.pos.y = clip(a.pos.y, a.radius, MyPanel.HEIGHT - a.radius);
    }

    /** Limits particle's position radius from screen's center */
    public static void applyConstraintsCircle(Particle a) {
        Vec2 center = new Vec2(MyPanel.WIDTH/2, MyPanel.HEIGHT/2);
        double radius = 300;
        Vec2 dir = Vec2.sub(center, a.pos);
        double err = Vec2.len(dir) + a.radius;
        if (err > radius) {
            a.pos = Vec2.add(a.pos, Vec2.mul(Vec2.normalize(dir), (err - radius)));
        }
    }

    /** Limits particle's position to screen height, but does not limit x */
    public static void applyConstraintsPlain(Particle a) {
        Vec2 pos = a.pos;

        //if (pos.x < a.radius || pos.x + a.radius > MyPanel.WIDTH) a.vel = Vec2.reflect(Vec2.mul(a.vel, energyLoss), new Vec2(1, 0));
        if (pos.y < a.radius || pos.y + a.radius > MyPanel.HEIGHT) a.vel = Vec2.reflect(Vec2.mul(a.vel, energyLoss), new Vec2(0, 1));

        if (a.pos.x > MyPanel.WIDTH) a.pos.x = a.pos.x - MyPanel.WIDTH;
        if (a.pos.x < 0) a.pos.x = a.pos.x + MyPanel.WIDTH;
        a.pos.y = clip(a.pos.y, a.radius, MyPanel.HEIGHT - a.radius);
    }

    /** Particle falls down
     * @see Physics#G*/
    public static void applyGravity(Particle a) { a.acc = Vec2.add(a.acc, new Vec2(0, G)); }

    /** Particle starts flying up if hot
     * @see Physics#heatUpwardForce*/
    public static void applyHeatUpwardForce(Particle a) { a.acc = Vec2.add(a.acc, new Vec2(0, -heatUpwardForce * a.T)); }

    /** Particle is cooling down with time
     * @see Physics#heatDiffusionSpeed*/
    public static void looseHeat(Particle a, double dt) {
        //a.T -= dt * heatDiffusionSpeed * a.T;
        a.T -= dt * heatDiffusionSpeed;
    }

    /** Increase particle's temperature if Y is below something
     * @see Physics#heatIfLow*/
    public static void addHeatIfLow(Particle a, double dt) {
        if (a.pos.y >= MyPanel.HEIGHT - a.radius * 1.1)
            //if (a.pos.x > MyPanel.WIDTH/2 - 70 && a.pos.x < MyPanel.WIDTH/2 + 70) // for making smaller heat surface
                a.T += heatIfLow * dt;
    }

    /** Change particle's temperature on collision
     * @see Physics#heatTransferSpeed*/
    public static void solveHeat(Particle a, Particle b) {
        double diff = a.T - b.T;
        a.T -= diff * heatTransferSpeed;
        b.T += diff * heatTransferSpeed;
    }

    /** Limit particle's speed
     * @see Physics#speedLimit*/
    public static void applySpeedLimit(Particle p) {
        double vel = Vec2.len(p.vel);
        if (vel > speedLimit) p.vel = Vec2.mul(Vec2.normalize(p.vel), speedLimit);
    }
}
