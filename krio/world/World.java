
package krio.world;

import krio.Configuration;
import krio.MyPanel;
import krio.buffer.NeighboursBuffer;
import krio.thread.MyTreadPool;
import krio.thread.MyThread;
import krio.util.Vec2;
import krio.world.physics.CollisionSolver;
import krio.world.physics.Physics;

import static krio.util.GenerateRandom.getRandomInt;

import java.awt.*;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ArrayList;

/**
 * Singleton-like class, containing all world's information, such as:
 * <li>particles see: {@link Particle}</li>
 * <li>neighbours buffer see: {@link NeighboursBuffer}</li>
 * <li>particle amount, physics recalculation count</li>
 * <li>{@link MyTreadPool} for calculating physics</li>
 */
public class World {
    public LinkedHashMap<Integer, BufferedParticle> particles;
    public NeighboursBuffer buffer;
    public List<Integer> idSortedList;
    public List<Integer> temperatureSortedList;

    public static double particleRad    = (double)Configuration.properties.get("particleRad");
    public static double bufferAccuracy = (double)Configuration.properties.get("bufferAccuracy"); // minimal is 1 / particleRad, higher buffer accuracy increases stability but lowers performance
    public int particleAmount           =    (int)Configuration.properties.get("particleAmount");
    public int dtRecalcIterations       =    (int)Configuration.properties.get("dtRecalcIterations");  // subiterations of physics like run(dt/dtRecalcIterations)

    private MyTreadPool threadPool;

    public World(){
        particles = new LinkedHashMap<Integer, BufferedParticle>();
        buffer = new NeighboursBuffer(MyPanel.WIDTH,
                                   MyPanel.HEIGHT,
                                   (int)(MyPanel.WIDTH * bufferAccuracy),
                                   (int)(MyPanel.HEIGHT * bufferAccuracy));
        BufferedParticle p;
        for (int i = 0; i < particleAmount; ++i) {
            p = new BufferedParticle(getRandomInt(0, MyPanel.WIDTH), getRandomInt(MyPanel.HEIGHT / 2, MyPanel.HEIGHT), 0, 0, particleRad, 1);
            particles.put(p.id, p);
        }

        idSortedList = new ArrayList<Integer>(particles.keySet());
        temperatureSortedList = new ArrayList<Integer>(particles.keySet());

        ArrayList<MyThread> threadList = new ArrayList<>();

        int startIndex1 = 0,                           endIndex1 = idSortedList.size() / 3;
        int startIndex2 = 1 * idSortedList.size() / 3, endIndex2 = 2 * idSortedList.size() / 3;
        int startIndex3 = 2 * idSortedList.size() / 3, endIndex3 = idSortedList.size();

        MyThread t1 = new MyThread(particles, buffer, idSortedList, startIndex1, endIndex1);
        MyThread t2 = new MyThread(particles, buffer, idSortedList, startIndex2, endIndex2);
        MyThread t3 = new MyThread(particles, buffer, idSortedList, startIndex3, endIndex3);

        threadList.add(t1);
        threadList.add(t2);
        threadList.add(t3);

        threadPool = new MyTreadPool(threadList);

        // Pre-solve collisions before start
        for (int key : particles.keySet()) {
            BufferedParticle pr = particles.get(key);
            pr.bufferedPos = buffer.add(pr);
            pr.isBuffered = pr.bufferedPos != null;
        }
        CollisionSolver.solve(particles, buffer, idSortedList, 0, idSortedList.size());
        for (int key : particles.keySet()) {
            BufferedParticle pr = particles.get(key);
            pr.prevPos = new Vec2(pr.pos.x, pr.pos.y);
        }
    }

    /**
     * Used in {@link MyPanel#paint(Graphics)}
     * for recalculating all particles positions and solving collisions by using:
     * <li>{@link Particle#move(double)}</li>
     * <li>{@link CollisionSolver}</li>
     * <li>{@link MyTreadPool#launch()}</li>
     * @param dt
     */
    public void run(double dt) {
        dt =  Physics.timeSpeed * dt / dtRecalcIterations;
        CollisionSolver.solvedCount = 0;

        // sort particles by pos.x for threading
        Collections.sort(idSortedList, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return Double.compare(particles.get(i1).pos.x, particles.get(i2).pos.x);
            }
        });

        buffer.reset();
        for (int key : idSortedList) {
            BufferedParticle p = particles.get(key);
            p.bufferedPos = buffer.add(p);
            p.isBuffered = p.bufferedPos != null;
            p.acc = new Vec2(0, 0);
        }

        for (int dtRec = 0; dtRec < dtRecalcIterations; ++dtRec) {
            for (int key : idSortedList) {
                BufferedParticle p = particles.get(key);
                Physics.applyGravity(p);
                Physics.addHeatIfLow(p, dt);
                Physics.applyHeatUpwardForce(p);
                Physics.looseHeat(p, dt);
                Physics.applyConstraintsBox(p);
                Physics.applySpeedLimit(p);
                p.move(dt);
            }

            // new way with threads
            threadPool.launch(); // launch collisionSolver.solve() on all threads
            while (!threadPool.isReady());

            // old way without threads
            //CollisionSolver.solve(particles, buffer, idSortedList, 0, idSortedList.size());

            // old way without buffer
            //CollisionSolver.solve(particles);
        }
    }
}
